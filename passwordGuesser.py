import tkinter as tk
from tkinter import simpledialog
import hashlib
from itertools import permutations
from yattag import Doc
import webbrowser
import os
import time

#letters_to_words = {}
#takes passwords trom text file and places them into array
passwordList = []
with open('dict.txt') as my_file:
	for line in my_file:
		word = line.strip()
		passwordList.append(word)
		# length = len(word)
		# if length in letters_to_words:
		# 	letters_to_words[length].append(word)
		# else:
		# 	letters_to_words[length]= [word]


#print(letters_to_words[5])

#uses tkinter to take password as input
application_window = tk.Tk()
password = simpledialog.askstring("Input", "What is your password?")
print(password)

passwordHash = hashlib.sha256(password.encode()) #256 Hash
#passwordHash = hashlib.sha512(password.encode()) #512 Hash

passLen = len(password)
print(len(passwordList))

# find = False
# for possible in iLengthCombos:
# 	if(len(possible) == len(password)):
# 		if(passwordHash.hexdigest() == hashlib.sha256(password.encode()).hexdigest()):  #for 256 hash
# 			print("found")
# 			find =True
# 			break

			# if(passwordHash == hashlib.sha512(password.encode())): #for 512 hash
			# 	find =True
			# 	break
# 	if find:
# 		break

start_time = time.time()
find = False
counter = 0
#runs out of memory so we need to fix this
for i in range(1, len(passwordList)): #the number of words from passwordList to make up passwaord
	iLengthCombos = ["".join(a) for a in permutations(passwordList, i)] #list of all i word combinations
	for possible in iLengthCombos:
		counter += 1
		if(len(possible) == len(password)):
			if(passwordHash.hexdigest() == hashlib.sha256(password.encode()).hexdigest()):  #for 256 hash
				print("found", possible)
				find =True
				break
			# if(passwordHash == hashlib.sha512(password.encode())): #for 512 hash
			# 	find =True
			# 	break
	if find:
		break

elapsed_time = time.time() - start_time
print("time elapsed: " + str(elapsed_time))

doc, tag, text = Doc().tagtext()

with tag('html'):
	with tag('body'):
		with tag('h3'):
			text('Hash Type: ')
		with tag('h3'):
			text('Dictionary Size: {}'.format(len(passwordList)))
		with tag('h3'):
			text('Time: {0:.3f}s'.format(elapsed_time))
		with tag('h3'):
			text('Number of Guesses: {}'.format(counter))
result = doc.getvalue()
path = os.path.abspath('temp.html')
url = 'file://' + path

with open(path, 'w') as f:
    f.write(result)
webbrowser.open(url)