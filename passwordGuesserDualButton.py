import tkinter as tk
from tkinter import simpledialog
import hashlib
from itertools import permutations
from yattag import Doc
import webbrowser
import os
import time

fields = ('Password')

def sha256Calc(entries):
    password = entries["Password"].get()

    passwordHash = hashlib.sha256(password.encode()) #256 Hash
    #passwordHash = hashlib.sha512(password.encode()) #512 Hash

    passLen = len(password)
    print(len(passwordList))

    start_time = time.time()
    find = False
    counter = 0
    #runs out of memory so we need to fix this
    for i in range(1, len(passwordList)): #the number of words from passwordList to make up passwaord
        iLengthCombos = ["".join(a) for a in permutations(passwordList, i)] #list of all i word combinations
        for possible in iLengthCombos:
            counter += 1
            if(len(possible) == len(password)):
                if(passwordHash.hexdigest() == hashlib.sha256(possible.encode()).hexdigest()):  #for 256 hash
                    print("found", possible)
                    find =True
                    break
                # if(passwordHash == hashlib.sha512(password.encode())): #for 512 hash
                #   find =True
                #   break
        if find:
            break

    elapsed_time = time.time() - start_time
    print("time elapsed: " + str(elapsed_time))

    doc, tag, text = Doc().tagtext()

    with tag('html'):
        with tag('body'):
            with tag('h3'):
                text('Hash Type: SHA256')
            with tag('h3'):
                text('Dictionary Size: {}'.format(len(passwordList)))
            with tag('h3'):
                text('Time: {0:.3f}s'.format(elapsed_time))
            with tag('h3'):
                text('Number of Guesses: {}'.format(counter))
    result = doc.getvalue()
    path = os.path.abspath('temp.html')
    url = 'file://' + path

    with open(path, 'w') as f:
        f.write(result)
    webbrowser.open(url)
    root.quit()
def sha512Calc(entries):
    password = entries["Password"].get()

    passwordHash = hashlib.sha512(password.encode()) #512 Hash

    passLen = len(password)
    print(len(passwordList))

    start_time = time.time()
    find = False
    counter = 0
    #runs out of memory so we need to fix this
    for i in range(1, len(passwordList)): #the number of words from passwordList to make up passwaord
        iLengthCombos = ["".join(a) for a in permutations(passwordList, i)] #list of all i word combinations
        for possible in iLengthCombos:
            counter += 1
            if(len(possible) == len(password)):
                if(passwordHash.hexdigest() == hashlib.sha512(possible.encode()).hexdigest()): #for 512 hash
                  find =True
                  break
        if find:
            break

    elapsed_time = time.time() - start_time
    print("time elapsed: " + str(elapsed_time))

    doc, tag, text = Doc().tagtext()

    with tag('html'):
        with tag('body'):
            with tag('h3'):
                text('Hash Type: SHA512')
            with tag('h3'):
                text('Dictionary Size: {}'.format(len(passwordList)))
            with tag('h3'):
                text('Time: {0:.3f}s'.format(elapsed_time))
            with tag('h3'):
                text('Number of Guesses: {}'.format(counter))
    result = doc.getvalue()
    path = os.path.abspath('temp.html')
    url = 'file://' + path

    with open(path, 'w') as f:
        f.write(result)
    webbrowser.open(url)
    root.quit()

def makeform(root, fields):
    entries = {}

    row = tk.Frame(root)
    lab = tk.Label(row, width=22, text="Password: ", anchor='w')
    ent = tk.Entry(row)
    ent.insert(0, "0")
    row.pack(side=tk.TOP, 
             fill=tk.X, 
             padx=5, 
             pady=5)
    lab.pack(side=tk.LEFT)
    ent.pack(side=tk.RIGHT, 
             expand=tk.YES, 
             fill=tk.X)
    entries["Password"] = ent
    return entries

if __name__ == '__main__':
    passwordList = []
    with open('dict.txt') as my_file:
        for line in my_file:
            word = line.strip()
            passwordList.append(word)



    root = tk.Tk()
    ents = makeform(root, fields)
    b1 = tk.Button(root, text='SHA256',
           command=(lambda e=ents: sha256Calc(e)))
    b1.pack(side=tk.LEFT, padx=5, pady=5)
    b2 = tk.Button(root, text='SHA512',
           command=(lambda e=ents: sha512Calc(e)))
    b2.pack(side=tk.LEFT, padx=5, pady=5)
    b3 = tk.Button(root, text='Quit', command=root.quit)
    b3.pack(side=tk.LEFT, padx=5, pady=5)
    root.mainloop()